import 'dart:ui';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget PersonalDetail = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 8,
                ),
              ),
              SizedBox(
                height: 200,
                width: 800,
                child: Card(
                  child: Column(
                    children: [
                      ListTile(
                        leading: Icon(
                          Icons.people,
                          color: Colors.blueGrey,
                          size: 40,
                        ),
                        title: const Text(
                          'ABOUT ME',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blueGrey,
                              fontSize: 25),
                        ),
                      ),
                      ListTile(
                        title: const Text(
                            "I am Saturat Minding, my nickname is Non, who has a strong passion and interest for programming. i wish programming will can monney is the most."),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 150,
                width: 800,
                child: Card(
                  child: Column(
                    children: [
                      ListTile(
                        leading: Icon(
                          Icons.school,
                          color: Colors.blueGrey,
                          size: 40,
                        ),
                        title: const Text(
                          'STUDINGING',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blueGrey,
                              fontSize: 25),
                        ),
                      ),
                      ListTile(
                        title: const Text(
                            "- 2018 - 2021 COMPUTER SCIENCE BURAPHA UNIVERSITY\n" +
                                "- 2012 - 2018 VISUTTHARANGSI SCHOOL"),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 150,
                width: 800,
                child: Card(
                  child: Column(
                    children: [
                      ListTile(
                        leading: Icon(
                          Icons.date_range,
                          color: Colors.blueGrey,
                          size: 40,
                        ),
                        title: const Text(
                          'PERSONAL DATE',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blueGrey,
                              fontSize: 25),
                        ),
                      ),
                      ListTile(
                        title: const Text("Gender : Male\n" +
                            "Age : 22 Year\n" +
                            "Date of birth : 22 July 1999\n" +
                            "Nationality : Thai"),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 250,
                width: 800,
                child: Card(
                  child: Column(
                    children: [
                      ListTile(
                        leading: Icon(
                          Icons.person,
                          color: Colors.blueGrey,
                          size: 40,
                        ),
                        title: const Text(
                          'CONTACT',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blueGrey,
                              fontSize: 25),
                        ),
                      ),
                      ListTile(
                        title: const Text("086 - 1646522"),
                        leading: Icon(
                          Icons.contact_phone,
                          color: Colors.blueGrey,
                          size: 30,
                        ),
                      ),
                      ListTile(
                        title: const Text("61160167@go.buu.ac.th"),
                        leading: Icon(
                          Icons.contact_mail,
                          color: Colors.blueGrey,
                          size: 30,
                        ),
                      ),
                      ListTile(
                        title: const Text("FaceBook Satarat Mindang"),
                        leading: Icon(
                          Icons.contact_page,
                          color: Colors.blueGrey,
                          size: 30,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )),
        ],
      ),
    );

    Widget Skills = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Image.asset(
            'images/html.png',
            width: 60,
            height: 60,
            fit: BoxFit.contain,
          ),
          Image.asset(
            'images/css.png',
            width: 60,
            height: 60,
            fit: BoxFit.contain,
          ),
          Image.asset(
            'images/java.png',
            width: 60,
            height: 60,
            fit: BoxFit.contain,
          ),
        ],
      ),
    );

    return MaterialApp(
        title: ('RESUME APP'),
        home: Scaffold(
          appBar: AppBar(
            title: const Text('RESUME APP'),
            backgroundColor: Colors.cyan[900],
          ),
          backgroundColor: Colors.blueGrey[800],
          body: ListView(children: [
            Image.asset(
              'images/ME.jpg',
              width: 440,
              height: 600,
              fit: BoxFit.cover,
            ),
            PersonalDetail,
            Container(
              margin: EdgeInsets.only(top: 5.0),
              child: Text(
                "SKILL",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Skills,
          ]),
        ));
  }
}
